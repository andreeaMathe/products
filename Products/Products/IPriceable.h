#pragma once
#include <cstdint>

struct IPriceable
{
	virtual ~IPriceable() = default;
	virtual int32_t GetVat() const = 0;
	virtual float GetPrice() const = 0;
};
